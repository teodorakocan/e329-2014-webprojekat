﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Xml.Linq;

namespace MyFirstMVCWebApp.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public string DateOfBDay { get; set; }
        public string Role { get; set; }
        public string MyManifestations { get; set; } //za prodavce
        public string Type { get; set; } //za kupce, vrsta kupca regular, vip, gold, server
        public string Tickets { get; set; }//za kupca-rezervisane karte
        public double Points { get; set; }
    }
}