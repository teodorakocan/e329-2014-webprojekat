﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstMVCWebApp.Models
{
    public class Tickets
    {
        public string ID { get; set; }//id karte min 10karaktera
        public string ReseverdManifestation { get; set; }
        public string DateAndTimeManifestation { get; set; }
        public double Price { get; set; }
        public string CustomerName { get; set; }
        public string Status { get; set; }  //reservisana ili odbijena
        public string Type { get; set; }//VIP, REGULAR, FUNPIT
    }
}