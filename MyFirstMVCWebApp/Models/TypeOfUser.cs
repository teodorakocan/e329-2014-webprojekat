﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstMVCWebApp.Models
{
    public class TypeOfUser
    {
        public string Name { get; set; }
        public int Discount { get; set; }
        public int Points { get; set; }
    }
}