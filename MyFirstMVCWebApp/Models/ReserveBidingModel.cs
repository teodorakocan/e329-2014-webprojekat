﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstMVCWebApp.Models
{
    public class ReserveBidingModel
    {
        public int NumberOfFanpit { get; set; }
        public int NumberOfVIP { get; set; }
        public int NumberOfRegular { get; set; }
        public double Total_price { get; set; }
        public string NameOfManifest { get; set; }
        public int TotalTickets {get;set;}
    }
}