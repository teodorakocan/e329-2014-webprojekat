﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstMVCWebApp.Models
{
    public class Manifestation
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int NumberOfPlaces { get; set; }
        public int NumberOfTickets { get; set; }
        public string DateOfManifestation { get; set; }
        public string TimeOfManifestation { get; set; }
        public double PriceOfTicket { get; set; }
        public string Status { get; set; }
        public Place PlaceOfManifestation { get; set; }
        public string Image { get; set; }
    }
}