﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstMVCWebApp.Models
{
    public class Place
    {
        public string Street_Num { get; set; }
        public string City { get; set; }
        public int ZipCode { get; set; }
    }
}