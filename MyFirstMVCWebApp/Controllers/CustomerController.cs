﻿using MyFirstMVCWebApp.Models;
using MyFirstMVCWebApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Xml.Linq;

namespace MyFirstMVCWebApp.Controllers
{
    [Authorize]
    [RoutePrefix("customer")]
    public class CustomerController : ApiController
    {
        [HttpPost]
        [Route("reserve")]
        [Authorize(Roles = "Customer")]
        public string ReserveTickets(ReserveBidingModel rbm)
        {
            var service = new UserService();
            Manifestation manifestation = new Manifestation();
            manifestation = service.GetManifestation(rbm.NameOfManifest);
            if (manifestation.NumberOfTickets < rbm.TotalTickets)
            {
                return "Only " + manifestation.NumberOfTickets + " tickets are left.";
            }

            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            string startElement = "Manifestations";
            string childElement = "Manifestation";

            var doc = XDocument.Load(path);
            var manifest = doc.Element(startElement).Elements(childElement)
                 .Where(e => e.Element("Name").Value.Equals(rbm.NameOfManifest)).Single();

            manifest.Element("NumberOfTickets").Value = (manifestation.NumberOfTickets - rbm.TotalTickets).ToString();
            doc.Save(path);

            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            User user = new User();
            user = service.SearchByUsername(claimsIdentity.Name);
            string path_Customer = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Customers.xml";
            string startElement_C = "Customers";
            string childElement_C = "Customer";
            var doc_Customer = XDocument.Load(path_Customer);

            string path_ticket = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Tickets.xml";
            XDocument doc_ticket = XDocument.Load(path_ticket);
            XElement newTicket = doc_ticket.Element("Tickets");

            if (rbm.NumberOfFanpit != 0)
            {
                for(int i = 0; i < rbm.NumberOfFanpit; i++)
                {
                    double price = service.GetPrice(user.Type, (manifestation.PriceOfTicket * 2));
                    string id_ticket = Guid.NewGuid().ToString("N").Substring(0, 10);
                    newTicket.Add(
                        new XElement("Ticket",
                        new XElement("ID", id_ticket),
                        new XElement("ReseverdManifestation", manifestation.Name),
                        new XElement("DateAndTimeManifestation", manifestation.DateOfManifestation + " " + manifestation.TimeOfManifestation),
                        new XElement("Price", price),
                        new XElement("CustomerName", user.Name + " " + user.Surname),
                        new XElement("Status", "Rezervisano"),
                        new XElement("Type", "Fan Pit")
                    ));

                    var tickets = doc_Customer.Element(startElement_C).Elements(childElement_C)
                         .Where(e => e.Element("Username").Value.Equals(claimsIdentity.Name)).Single();

                    if(tickets.Element("Tickets").Value == "")
                    {
                        tickets.Element("Tickets").Value += id_ticket;
                    }
                    else
                    {
                        tickets.Element("Tickets").Value = tickets.Element("Tickets").Value +"," + id_ticket;
                    }
                }
            }

            if (rbm.NumberOfRegular != 0)
            {
                for (int i = 0; i < rbm.NumberOfRegular; i++)
                {
                    double price = service.GetPrice(user.Type, manifestation.PriceOfTicket);
                    string id_ticket = Guid.NewGuid().ToString("N").Substring(0, 10);
                    newTicket.Add(
                        new XElement("Ticket",
                        new XElement("ID", id_ticket),
                        new XElement("ReseverdManifestation", manifestation.Name),
                        new XElement("DateAndTimeManifestation", manifestation.DateOfManifestation + " " + manifestation.TimeOfManifestation),
                        new XElement("Price", price),
                        new XElement("CustomerName", user.Name + " " + user.Surname),
                        new XElement("Status", "Rezervisano"),
                        new XElement("Type", "Regular")
                    ));

                    var tickets = doc_Customer.Element(startElement_C).Elements(childElement_C)
                         .Where(e => e.Element("Username").Value.Equals(claimsIdentity.Name)).Single();

                    if (tickets.Element("Tickets").Value == "")
                    {
                        tickets.Element("Tickets").Value = id_ticket;
                    }
                    else
                    {
                        tickets.Element("Tickets").Value = tickets.Element("Tickets").Value + "," + id_ticket;
                    }
                }
            }

            if (rbm.NumberOfVIP != 0)
            {
                for (int i = 0; i < rbm.NumberOfVIP; i++)
                {
                    double price = service.GetPrice(user.Type, (manifestation.PriceOfTicket*4));
                    string id_ticket = Guid.NewGuid().ToString("N").Substring(0, 10);
                    newTicket.Add(
                        new XElement("Ticket",
                        new XElement("ID", id_ticket),
                        new XElement("ReseverdManifestation", manifestation.Name),
                        new XElement("DateAndTimeManifestation", manifestation.DateOfManifestation + " " + manifestation.TimeOfManifestation),
                        new XElement("Price", price),
                        new XElement("CustomerName", user.Name + " " + user.Surname),
                        new XElement("Status", "Rezervisano"),
                        new XElement("Type", "VIP")
                    ));

                    var tickets = doc_Customer.Element(startElement_C).Elements(childElement_C)
                         .Where(e => e.Element("Username").Value.Equals(claimsIdentity.Name)).Single();

                    if (tickets.Element("Tickets").Value == "")
                    {
                        tickets.Element("Tickets").Value = id_ticket;
                    }
                    else
                    {
                        tickets.Element("Tickets").Value = tickets.Element("Tickets").Value + "," + id_ticket;
                    }
                }
            }

            doc_Customer.Save(path_Customer);
            doc_ticket.Save(path_ticket);
            UpdatePoints(rbm.NameOfManifest,rbm.NumberOfVIP, rbm.NumberOfRegular, rbm.NumberOfFanpit, user.Username);
            
            return "You have successfully booked tickets. ";
        }

        private void UpdatePoints(string name, int fan_pit, int vip, int regular, string username)
        {
            var service = new UserService();
            Manifestation manifestation = new Manifestation();
            manifestation = service.GetManifestation(name);
            User user = new User();
            user = service.SearchByUsername(username);
            TypeOfUser type = new TypeOfUser();
            type = service.SearchUserType(user.Type);

            double price_fanpit = manifestation.PriceOfTicket * 4;
            double price_vip = manifestation.PriceOfTicket * 2;
            double points = 0;
            double discount = 0;
            
            for(int i = 0; i<fan_pit; i++)
            {
                double total_price = 0;
                discount = price_fanpit / 100 * type.Discount;
                total_price += price_fanpit - discount;
                points += total_price / 1000 * 133;
            }
            for(int i = 0; i < vip; i++)
            {
                double total_price = 0;
                discount = price_vip / 100 * type.Discount;
                total_price += price_vip - discount;
                points += total_price / 1000 * 133;
            }
            for (int i = 0; i < regular; i++)
            {
                points += manifestation.PriceOfTicket / 1000 * 133;
            }

            string user_type;
            if(points >= 3000)
            {
                user_type = "Bronze";
            }
            else if(7000 <= points)
            {
                user_type = "Silver";
            }
            else if(10000 <= points)
            {
                user_type = "Gold";
            }
            else
            {
                user_type = "Regular";
            }

            string path_Customer = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Customers.xml";
            string startElement_C = "Customers";
            string childElement_C = "Customer";
            var doc_Customer = XDocument.Load(path_Customer);

            var newpoints = doc_Customer.Element(startElement_C).Elements(childElement_C)
                         .Where(e => e.Element("Username").Value.Equals(username)).Single();
            newpoints.Element("Points").Value = points.ToString();
            newpoints.Element("Type").Value = user_type;
            doc_Customer.Save(path_Customer);
        }
        
        [HttpGet]
        [Route("getusertype")]
        [Authorize(Roles = "Customer")]
        public TypeOfUser UserInfo()
        {
            var service = new UserService();
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            User user = new User();
            user = service.SearchByUsername(claimsIdentity.Name);
            TypeOfUser type = new TypeOfUser();
            type = service.SearchUserType(user.Type);
            return type;
        }

        [HttpGet]
        [Authorize(Roles ="Customer")]
        [Route("mytickets")]
        public List<Tickets> AllMyTickets()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var service = new UserService();
            User user = new User();
            user = service.SearchByUsername(claimsIdentity.Name);
            List<Tickets> tickets = new List<Tickets>();
            List<Tickets> user_tickets = new List<Tickets>();
            tickets = service.GetAllTickets();
            string full_name = user.Name + " " + user.Surname;

            foreach(Tickets ticket in tickets)
            {
                if (ticket.CustomerName.Equals(full_name))
                {
                    if (ticket.Status.Equals("Rezervisano"))
                    {
                        user_tickets.Add(ticket);
                    }
                }
            }

            return user_tickets;
        }

        [HttpPut]
        [Authorize(Roles ="Customer")]
        [Route("cancelticket")]
        public string CancelTicket(Tickets ticket)
        {
            string[] date_time = ticket.DateAndTimeManifestation.Split(' ');
            string[] dates = date_time[0].Split('-');
            string date = dates[1] + "/" + dates[2] + "/" + dates[0];
            if (DateTime.Parse(date) < DateTime.Now.AddDays(-7))
            {
                string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Tickets.xml";
                string startElement = "Tickets";
                string childElement = "Ticket";
                var doc = XDocument.Load(path);

                var newstatus = doc.Element(startElement).Elements(childElement)
                             .Where(e => e.Element("ID").Value.Equals(ticket.ID)).Single();
                newstatus.Element("Status").Value = "Otkazana";

                doc.Save(path);

                var service = new UserService();
                ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
                User user = new User();
                user = service.SearchByUsername(claimsIdentity.Name);

                string path_C = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Customers.xml";
                string startElement_C = "Customers";
                string childElement_C = "Customer";
                var doc_C = XDocument.Load(path_C);

                double update_points = ticket.Price / 1000 * 1333 * 4;
                double total_points = user.Points - update_points;
                var newpoints = doc_C.Element(startElement_C).Elements(childElement_C)
                             .Where(e => e.Element("Username").Value.Equals(claimsIdentity.Name)).Single();
                newpoints.Element("Points").Value = total_points.ToString();

                doc.Save(path);
                return "You canceled ticket for manifestation " + ticket.ReseverdManifestation;
            }
            return "You can't cancel ticket for manifestation " + ticket.ReseverdManifestation + ".You can only cancle ticket 7 days before manifestation start";
        } 
    }
}
