﻿using MyFirstMVCWebApp.Models;
using MyFirstMVCWebApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;

namespace MyFirstMVCWebApp.Controllers
{
    [Authorize]
    [RoutePrefix("admin")]
    public class AdminController : ApiController
    {
        [HttpGet]
        [Route("getallusers")]
        [Authorize(Roles ="Admin")]
        public List<User> getAllUsers()
        {
            var service = new UserService();
            List<User> list = new List<User>();
            list = service.GetSellersCustomers();
            return list;
        }

        [HttpPut]
        [Route("deleteuser")]
        [Authorize(Roles = "Admin")]
        public string DeleteUser(User u)
        {
            string path = "";
            if (u.Role.Equals("Customer"))
            {
                path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Customers.xml";
            }
            else
            {
                path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Sellers.xml";
            }

            XDocument doc = XDocument.Load(path);

            foreach (var node in doc.Descendants(u.Role))
            {
                if (node.Element("Username").Value.Equals(u.Username))
                {
                    node.Remove();
                    break;
                }
            }

            doc.Save(path);
            return "success";
        }

        [HttpPost]
        [Route("addseller")]
        [Authorize(Roles = "Admin")]
        public string AddSeller(User seller)
        {
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Sellers.xml";
            XDocument doc = XDocument.Load(path);
            XElement newSeller = doc.Element("Sellers");
            string userRole = "Seller";

            foreach (var node in doc.Descendants("Seller"))
            {
                if (node.Element("Username").Value.Equals(seller.Username))
                {
                    return "wrong";
                }
            }

            newSeller.Add(
                new XElement("Seller",
                new XElement("Username", seller.Username),
                new XElement("Password", seller.Password),
                new XElement("Name", seller.Name),
                new XElement("Surname", seller.Surname),
                new XElement("DateOfBirthday", seller.DateOfBDay),
                new XElement("Gender", seller.Gender),
                new XElement("Role", userRole),
                new XElement("MyManifestations", "")
                ));
            doc.Save(path);

            return "You have successful added new seller!";
        }

        [HttpGet]
        [Route("inactive")]
        [Authorize(Roles ="Admin")]
        public List<Manifestation> GetInactivManifestations()
        {
            List<Manifestation> manifestatins = new List<Manifestation>();
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            List<Manifestation> list = new List<Manifestation>();
            XDocument doc = XDocument.Load(path);

            foreach (var node in doc.Descendants("Manifestation"))
            {
                if(node.Element("Status").Value.Equals("Inactive"))
                {
                    Manifestation manifestation = new Manifestation()
                    {
                        Name = node.Element("Name").Value,
                        Type = node.Element("Type").Value,
                        NumberOfPlaces = Int32.Parse(node.Element("NumberOfPlaces").Value),
                        NumberOfTickets = Int32.Parse(node.Element("NumberOfTickets").Value),
                        DateOfManifestation = node.Element("DateOfManifestation").Value,
                        PriceOfTicket = double.Parse(node.Element("PriceOfTicket").Value),
                        TimeOfManifestation = node.Element("TimeOfManifestation").Value,
                        Status = node.Element("Status").Value,
                        PlaceOfManifestation = new Place(),
                        Image = node.Element("Image").Value
                    };

                    string placeOfManifest = node.Element("PlaceOfManifestation").Value;
                    string[] place = placeOfManifest.Split(',');
                    manifestation.PlaceOfManifestation.Street_Num = place[1];
                    manifestation.PlaceOfManifestation.City = place[0];
                    manifestation.PlaceOfManifestation.ZipCode = Int32.Parse(place[2]);

                    list.Add(manifestation);
                    list.Sort((x, y) => DateTime.Compare(DateTime.Parse(x.DateOfManifestation), DateTime.Parse(y.DateOfManifestation)));
                }
            }
            return list;
        }

        [HttpPut]
        [Route("active/{man_name}")]
        [Authorize(Roles ="Admin")]
        public void ChangeStatusOfManifestation([FromUri] string man_name)
        {
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            string startElement = "Manifestations";
            string childElement = "Manifestation";

            var doc = XDocument.Load(path);
            var status = doc.Element(startElement).Elements(childElement)
                 .Where(e => e.Element("Name").Value.Equals(man_name)).Single();

            status.Element("Status").Value = "Active";

            doc.Save(path);
        }

        [HttpGet]
        [Route("search")]
        [Authorize(Roles ="Admin")]
        public List<User> SerachUser(string name, string surname, string username)
        {
            var service = new UserService();
            List<User> list = new List<User>();
            List<User> searched = new List<User>();
            list = service.GetSellersCustomers();

            foreach(User user in list)
            {
                if(name != null)
                {
                    if (user.Name.ToLower().Equals(name.ToLower()) || user.Name.ToLower().Contains(name.ToLower()))
                    {
                        searched.Add(user);
                    }
                }
                if(surname !=null)
                {
                    if (user.Surname.ToLower().Equals(surname.ToLower()) || user.Surname.ToLower().Contains(surname.ToLower()))
                    {
                        searched.Add(user);
                    }
                }
                if(username != null)
                {
                    if (user.Username.ToLower().Equals(username.ToLower()) || user.Username.ToLower().Contains(username.ToLower()))
                    {
                        searched.Add(user);
                    }
                }
            }
            return searched;
        }

        [HttpGet]
        [Authorize(Roles ="Admin")]
        [Route("tickets")]
        public List<Tickets> AllTickets()
        {
            var service = new UserService();
            List<Tickets> all_tickets = new List<Tickets>();
            all_tickets = service.GetAllTickets();

            return all_tickets;
        }
    }
}