﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using MyFirstMVCWebApp.Models;
using MyFirstMVCWebApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;

namespace MyFirstMVCWebApp.Controllers
{
    [Authorize]
    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        [HttpPost]
        [Route("signup")]
        [AllowAnonymous]
        public string SignUp(User user)
        {
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Customers.xml";
            XDocument doc = XDocument.Load(path);
            XElement newUser = doc.Element("Customers");
            string userRole = "Customer";

            foreach (var node in doc.Descendants("Customer"))
            {
                if (node.Element("Username").Value.Equals(user.Username))
                {
                    return "wrong";
                }
            }

            newUser.Add(
                new XElement("Customer",
                new XElement("Username", user.Username),
                new XElement("Password", user.Password),
                new XElement("Name", user.Name),
                new XElement("Surname", user.Surname),
                new XElement("TypeOfUser", "Regular"),
                new XElement("DateOfBirthday", user.DateOfBDay),
                new XElement("Gender", user.Gender),
                new XElement("Role", userRole),
                new XElement("Points", 0),
                new XElement("Tickets", "")
                ));
            doc.Save(path);

            return "You hava successfully registered!";
        }
        
        [HttpGet]
        [Route("signin")]
        [AllowAnonymous]
        public string Sigin(string username, string password)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            User logged = new User();
            logged = SearchByUsername(username);

            return "succes";
        }

        [HttpPut]
        [Route("deleteaccount")]
        [Authorize (Roles = "Admin, Seller, Customer")]
        public void DeleteAccount()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var role = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\" + role[0] + "s.xml";
            string startElement = role[0]+"s";
            string childElement = role[0];

             XDocument doc = XDocument.Load(path);
             XElement newUser = doc.Element(startElement);

             foreach (var node in doc.Descendants(childElement))
             {
                 if (node.Element("Username").Value.Equals(claimsIdentity.Name))
                 {
                     node.Remove();
                     break;
                 }
             }
             doc.Save(path);
        }

        [HttpGet]
        [Route("getuserinfo")]
        [Authorize(Roles = "Admin, Seller, Customer")]
        public User UserInfo()
        {
            User user = new User();
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            user = SearchByUsername(claimsIdentity.Name);
            return user;
        }
        
        [HttpPut]
        [Route("changeprofile")]
        [Authorize(Roles = "Admin, Seller, Customer")]
        public string ChangeProfile(User user)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var role = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\" + role[0] + "s.xml";
            string startElement = role[0] + "s";
            string childElement = role[0];

            XDocument doc = XDocument.Load(path);
            XElement newUser = doc.Element(startElement);

            foreach (var node in doc.Descendants(childElement))
            {
                if (node.Element("Username").Value.Equals(claimsIdentity.Name))
                {
                    string password = node.Element("Password").Value;
                    node.Element("Name").Value = user.Name;
                    node.Element("Surname").Value = user.Surname;
                    node.Element("Password").Value = password;
                    node.Element("Username").Value = claimsIdentity.Name;
                    node.Element("DateOfBirthday").Value = user.DateOfBDay;
                    node.Element("Role").Value = role[0];
                    node.Element("Gender").Value = user.Gender;
                    break;
                }
            }
            
            doc.Save(path);
            return "succes";
        }

        [HttpPut]
        [Route("changepass")]
        [Authorize(Roles = "Admin, Seller, Customer")]
        public string ChangePassword([FromBody] string newpass)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var role = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\" + role[0] + "s.xml";
            string startElement = role[0] + "s";
            string childElement = role[0];

            var doc = XDocument.Load(path);
            var newPassword = doc.Element(startElement).Elements(childElement)
                 .Where(e => e.Element("Username").Value.Equals(claimsIdentity.Name)).Single();

            newPassword.Element("Password").Value = newpass;
            
            doc.Save(path);
            return "success";
        }

        #region Helpers
        private User SearchByUsername(string username)
        {
            var service = new UserService();
            List<User> list = new List<User>();
            list = service.GetUserList();

            foreach(User user in list)
            {
                if(user.Username.Equals(username))
                {
                    return user;
                }
            }
            return null;
        }

        
        #endregion
    }
}