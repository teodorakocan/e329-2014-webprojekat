﻿using MyFirstMVCWebApp.Models;
using MyFirstMVCWebApp.Service;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;

namespace MyFirstMVCWebApp.Controllers
{
    [Authorize]
    [RoutePrefix("seller")]
    public class SellerController : ApiController
    {
        [HttpPost]
        [Route("newmanifestation")]
        [Authorize(Roles ="Seller")]
        public string AddManifestation(Manifestation man)
        {
            man.Status = "Inactive";
            man.NumberOfTickets = man.NumberOfPlaces;

            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            List<Manifestation> listOfEvents = new List<Manifestation>();
            XDocument doc = XDocument.Load(path);
            XElement newManifestation = doc.Element("Manifestations");

            foreach (var node in doc.Descendants("Manifestation"))
            {
                if (node.Element("Name").Value.Equals(man.Name))
                {
                    return "There is already manifestation with such a name. Change the name."; //nesme se uneti manifestacija sa imenom koji vec postoji
                }
            }

            foreach (var node in doc.Descendants("Manifestation"))
            {
                string[] location = node.Element("PlaceOfManifestation").Value.Split(',');
                if(location[0].Equals(man.PlaceOfManifestation.City))
                {
                    if(location[1].Equals(man.PlaceOfManifestation.Street_Num))
                    {
                        if (node.Element("DateOfManifestation").Value == man.DateOfManifestation)
                        {
                            if (node.Element("TimeOfManifestation").Value == man.TimeOfManifestation)
                            {
                                return "Requested date and time in requested place is not available.Pleas change date/time or place";
                            }
                        }
                    }
                }
            }

            newManifestation.Add(
                new XElement("Manifestation",
                new XElement("Name", man.Name),
                new XElement("Type", man.Type),
                new XElement("NumberOfPlaces", man.NumberOfPlaces),
                new XElement("NumberOfTickets", man.NumberOfTickets),
                new XElement("DateOfManifestation", man.DateOfManifestation),
                new XElement("TimeOfManifestation", man.TimeOfManifestation),
                new XElement("PriceOfTicket", man.PriceOfTicket),
                new XElement("Status", man.Status),
                new XElement("PlaceOfManifestation", man.PlaceOfManifestation.City + "," + man.PlaceOfManifestation.Street_Num + "," + man.PlaceOfManifestation.ZipCode),
                new XElement("Image", man.Image)
                ));

            doc.Save(path);

            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;

            string path_Seller = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Sellers.xml";
            string startElement = "Sellers";
            string childElement = "Seller";

            var doc_Seller = XDocument.Load(path_Seller);
            var manifestations = doc_Seller.Element(startElement).Elements(childElement)
                  .Where(e => e.Element("Username").Value.Equals(claimsIdentity.Name)).Single();

            if (manifestations.Element("MyManifestations").Value == "")
            {
                manifestations.Element("MyManifestations").Value = man.Name;
            }
            else
            {
                manifestations.Element("MyManifestations").Value = manifestations.Element("MyManifestations").Value +"," + man.Name;
            }

            doc_Seller.Save(path_Seller);

            return "You have successfully added a new manifestation!";
        }

        [HttpPost]
        [Route("upload")]
        [Authorize(Roles = "Seller")]
        public void UploadFile()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), httpPostedFile.FileName);
                    httpPostedFile.SaveAs(fileSavePath);
                }
            }
        }

        [HttpGet]
        [Route("getMyManifestations")]
        [Authorize(Roles ="Seller")]
        public List<Manifestation> GetMyManifestations()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            List<Manifestation> list = new List<Manifestation>();
            string path_Seller = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Sellers.xml";
            XDocument doc_Seller = XDocument.Load(path_Seller);
            string myManifestations = "";
            
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            XDocument doc = XDocument.Load(path);

            foreach (var node in doc_Seller.Descendants("Seller"))
            {
                if (node.Element("Username").Value.Equals(claimsIdentity.Name))
                {
                    myManifestations = node.Element("MyManifestations").Value;
                    break;
                }
            }

            if(myManifestations == "")
            {
                return list;
            }
            else if(myManifestations.Contains(','))
            {
                string[] manifestations = myManifestations.Split(',');
                for(int i = 0; i < manifestations.Length; i ++)
                {
                    foreach (var node in doc.Descendants("Manifestation"))
                    {
                        if (node.Element("Name").Value.Equals(manifestations[i]))
                        {
                            Manifestation manifestation = new Manifestation()
                            {
                                Name = node.Element("Name").Value,
                                Type = node.Element("Type").Value,
                                NumberOfPlaces = Int32.Parse(node.Element("NumberOfPlaces").Value),
                                NumberOfTickets = Int32.Parse(node.Element("NumberOfTickets").Value),
                                DateOfManifestation = node.Element("DateOfManifestation").Value,
                                PriceOfTicket = double.Parse(node.Element("PriceOfTicket").Value),
                                TimeOfManifestation = node.Element("TimeOfManifestation").Value,
                                Status = node.Element("Status").Value,
                                PlaceOfManifestation = new Place(),
                                Image = node.Element("Image").Value
                            };
                            string placeOfManifest = node.Element("PlaceOfManifestation").Value;
                            string[] place = placeOfManifest.Split(',');
                            manifestation.PlaceOfManifestation.Street_Num = place[1];
                            manifestation.PlaceOfManifestation.City = place[0];
                            manifestation.PlaceOfManifestation.ZipCode = Int32.Parse(place[2]);

                            list.Sort((x, y) => DateTime.Compare(DateTime.Parse(x.DateOfManifestation), DateTime.Parse(y.DateOfManifestation)));
                            list.Add(manifestation);
                        }
                    }
                }
            }
            else
            {
                foreach (var node in doc.Descendants("Manifestation"))
                {
                    if (node.Element("Name").Value.Equals(myManifestations))
                    {
                        Manifestation manifestation = new Manifestation()
                        {
                            Name = node.Element("Name").Value,
                            Type = node.Element("Type").Value,
                            NumberOfPlaces = Int32.Parse(node.Element("NumberOfPlaces").Value),
                            NumberOfTickets = Int32.Parse(node.Element("NumberOfTickets").Value),
                            DateOfManifestation = node.Element("DateOfManifestation").Value,
                            PriceOfTicket = double.Parse(node.Element("PriceOfTicket").Value),
                            TimeOfManifestation = node.Element("TimeOfManifestation").Value,
                            Status = node.Element("Status").Value,
                            PlaceOfManifestation = new Place(),
                            Image = node.Element("Image").Value
                        };
                        string placeOfManifest = node.Element("PlaceOfManifestation").Value;
                        string[] place = placeOfManifest.Split(',');
                        manifestation.PlaceOfManifestation.Street_Num = place[1];
                        manifestation.PlaceOfManifestation.City = place[0];
                        manifestation.PlaceOfManifestation.ZipCode = Int32.Parse(place[2]);

                        list.Sort((x, y) => DateTime.Compare(DateTime.Parse(x.DateOfManifestation), DateTime.Parse(y.DateOfManifestation)));
                        list.Add(manifestation);
                    }
                }
            }

            return list;
        }

        [HttpGet]
        [Route("check/{name}")]
        [Authorize(Roles ="Seller")]
        public bool CheckManifestation(string name)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            List<string> list = new List<string>();
            string path_Seller = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Sellers.xml";
            XDocument doc_Seller = XDocument.Load(path_Seller);
            string myManifestations = "";
            foreach (var node in doc_Seller.Descendants("Seller"))
            {
                if (node.Element("Username").Value.Equals(claimsIdentity.Name))
                {
                    myManifestations = node.Element("MyManifestations").Value;
                }
            }

            if(myManifestations.Contains(','))
            {
                string[] manifestations = myManifestations.Split(',');
                for(int i = 0; i< manifestations.Length; i++)
                {
                    if(name.Equals(manifestations[i]))
                    {
                        return true;
                    }
                }
            }
            else
            {
                if(myManifestations.Equals(name))
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPut]
        [Route("changeManifestation")]
        [Authorize(Roles ="Seller")]
        public string ChangeManifestation(Manifestation manifest)
        {
            Manifestation man = new Manifestation();
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            XDocument doc = XDocument.Load(path);
            foreach (var node in doc.Descendants("Manifestation"))
            {
                if (!node.Element("Name").Value.Equals(manifest.Name))
                {
                    string[] location = node.Element("PlaceOfManifestation").Value.Split(',');
                    if (location[0].Equals(manifest.PlaceOfManifestation.City))
                    {
                        if (location[1].Equals(manifest.PlaceOfManifestation.Street_Num))
                        {
                            if (node.Element("DateOfManifestation").Value == manifest.DateOfManifestation)
                            {
                                if (node.Element("TimeOfManifestation").Value == manifest.TimeOfManifestation)
                                {
                                    return "Requested date and time in requested place is not available.Pleas change date/time or place";
                                }
                            }
                        }
                    }
                }
            }
            
            foreach (var node in doc.Descendants("Manifestation"))
            {
                if (node.Element("Name").Value.Equals(manifest.Name))
                {
                    string image = "";
                    if (manifest.Image == "")
                    {
                        image = node.Element("Image").Value;
                    }
                    else
                    {
                        image = manifest.Image;
                    }

                    node.Element("Name").Value = node.Element("Name").Value;
                    node.Element("Type").Value = manifest.Type;
                    node.Element("NumberOfPlaces").Value = manifest.NumberOfPlaces.ToString();
                    node.Element("NumberOfTickets").Value = node.Element("NumberOfTickets").Value;
                    node.Element("DateOfManifestation").Value = manifest.DateOfManifestation;
                    node.Element("PriceOfTicket").Value = manifest.PriceOfTicket.ToString();
                    node.Element("TimeOfManifestation").Value = manifest.TimeOfManifestation;
                    node.Element("Status").Value = node.Element("Status").Value;
                    node.Element("PlaceOfManifestation").Value = manifest.PlaceOfManifestation.City+","+ manifest.PlaceOfManifestation.Street_Num+","+ manifest.PlaceOfManifestation.ZipCode;
                    node.Element("Image").Value = image;
                    
                    doc.Save(path);
                    return "Manifetstaion is changed";
                }
            }
            return "Error";
        }

        [HttpGet]
        [Authorize(Roles = "Seller")]
        [Route("tickets")]
        public List<Tickets> AllTickets()
        {
            var service = new UserService();
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            User user = new User();
            user = service.SearchByUsername(claimsIdentity.Name);

            List<Tickets> tickets = new List<Tickets>();
            tickets = service.GetAllTickets();
            List<Tickets> list = new List<Tickets>();

            string[] manifestations = user.MyManifestations.Split(',');
            foreach(Tickets ticket in tickets)
            {
                for(int i = 0; i< manifestations.Length; i++)
                {
                    if(ticket.ReseverdManifestation.Equals(manifestations[i]))
                    {
                        list.Add(ticket);
                    }
                }
            }

            return list;
        }
    }
}
