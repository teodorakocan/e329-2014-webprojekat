﻿using MyFirstMVCWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Security.Claims;

namespace MyFirstMVCWebApp.Controllers
{
    public class ViewController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Contact()
        {
            ViewBag.Message = "GIGS TIX doo";

            return View();
        }
        
        public ActionResult SignUp()
        {
            if(HttpContext.Session["user"] == null)
            {
                return View();
            }
            else {
                return View("Index");
            }
        }
        
        public ActionResult SignIn()
        {
            return View();
        }
        
        public ActionResult SignOut()
        {
            return RedirectToAction("Index", "View");
        }
        
        public ActionResult AllUsers()
        {
            return View();
        }
        
        public ActionResult AddSeller()
        {
            return View();
        }
        
        public ActionResult ChangeProfile()
        {
            return View();
        }
        
        public ActionResult Profile()
        {
            return View();
        }
        
        public ActionResult Event()
        {
            return View();
        }
        
        public ActionResult AddManifestation()
        {
            return View();
        }

        public ActionResult Manifestations()
        {
            return View();
        }

        public ActionResult ChangeManifestaion()
        {
            return View();
        }

        public ActionResult Tickets()
        {
            return View();
        }
    }
}