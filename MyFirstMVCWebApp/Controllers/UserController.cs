﻿using MyFirstMVCWebApp.Models;
using MyFirstMVCWebApp.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MyFirstMVCWebApp.Controllers
{
    [Authorize]
    [RoutePrefix("user")]
    public class UserController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("getManifestations")]
        public List<Manifestation> getManifestations()
        {
            var service = new UserService();
            List<Manifestation> list = new List<Manifestation>();
            List<Manifestation> manifestations = new List<Manifestation>();
            list = service.GetManifestations();
            foreach (Manifestation man in list)
            {
                if(man.Status == "Active")
                {
                    manifestations.Add(man);
                }
            }

            return manifestations;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("search")]
        public List<Manifestation> manifestationsSearch(string name, string place, string date, string priceFrom, string priceTo)
        {
            List<Manifestation> requested = new List<Manifestation>();
            List<Manifestation> manifestations = new List<Manifestation>();
            manifestations = getManifestations();
            if (name != null)
            {
                foreach (Manifestation man in manifestations)
                {
                    if (man.Name.ToLower().Equals(name.ToLower()) || man.Name.ToLower().Contains(name.ToLower()))
                    {
                        requested.Add(man);
                    }
                }
            }

            if (place != null)
            {
                foreach (Manifestation man in manifestations)
                {
                    if (man.PlaceOfManifestation.City.ToLower().Equals(place.ToLower()) || man.PlaceOfManifestation.City.ToLower().Contains(place.ToLower()))
                    {
                        if (!requested.Contains(man))
                        {
                            requested.Add(man);
                        }
                    }
                }
            }

            string[] dates = date.Split('-');
            DateTime startDate = DateTime.Parse(dates[0]);
            DateTime endDate = DateTime.Parse(dates[1]);
            foreach (Manifestation man in manifestations)
            {
                DateTime nowDate = DateTime.Parse(man.DateOfManifestation);
                if (nowDate.Date >= startDate.Date && nowDate.Date <= endDate.Date)
                {
                    if (!requested.Contains(man))
                    {
                        requested.Add(man);
                    }
                }

            }

            if (priceFrom == null)
            {
                priceFrom = "0";
            }

            if (priceTo != null)
            {
                double lower_price = Double.Parse(priceFrom);
                double higher_price = Double.Parse(priceTo);
                foreach (Manifestation man in manifestations)
                {
                    if (man.PriceOfTicket >= lower_price && man.PriceOfTicket < higher_price)
                    {
                        if (!requested.Contains(man))
                        {
                            requested.Add(man);
                        }
                    }
                }
            }

            return requested;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("manifestation/{name}")]
        public Manifestation GetManifestatin(string name)
        {
            var service = new UserService();
            List<Manifestation> manifestations = new List<Manifestation>();
            manifestations = service.GetManifestations();
            foreach(Manifestation man in manifestations)
            {
                string n = man.Name;
                if (man.Name.Contains('.'))
                {
                    man.Name = n.Replace(".", "");
                }
                if (man.Name.Equals(name))
                {
                    Manifestation manifetstation = new Manifestation()
                    {
                        Name = man.Name,
                        Type = man.Type,
                        NumberOfPlaces = man.NumberOfPlaces,
                        NumberOfTickets = man.NumberOfTickets,
                        DateOfManifestation = man.DateOfManifestation,
                        PriceOfTicket = man.PriceOfTicket,
                        Status = man.Status,
                        PlaceOfManifestation = man.PlaceOfManifestation,
                        TimeOfManifestation = man.TimeOfManifestation,
                        Image = man.Image,
                    };
                    return manifetstation;
                }
            }
            return null;
        }
    }
}