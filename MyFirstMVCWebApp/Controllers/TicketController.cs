﻿using MyFirstMVCWebApp.Models;
using MyFirstMVCWebApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace MyFirstMVCWebApp.Controllers
{
    [Authorize]
    [RoutePrefix("ticket")]
    public class TicketController : ApiController
    {

        [HttpGet]
        [Authorize(Roles = "Admin, Seller, Customer")]
        [Route("search")]
        public List<Tickets> SerachTicket(string name, string date, string priceFrom, string priceTo)
        {
            var service = new UserService();
            List<Tickets> tickets = new List<Tickets>();
            List<Tickets> requested = new List<Tickets>();
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            User user = new User();
            user = service.SearchByUsername(claimsIdentity.Name);
            if(user.Role.Equals("Customer"))
            {
                string full_name = user.Name + " " + user.Surname;
                tickets = service.GetCustomerTickets(full_name);
            }
            else if(user.Role.Equals("Seller"))
            {
                string[] manifestations;
                if(user.MyManifestations.Contains(','))
                {
                    manifestations = user.MyManifestations.Split(',');
                    for(int i=0; i< manifestations.Length; i++)
                    {
                        List<Tickets> all_tickets = new List<Tickets>();
                        all_tickets = service.GetSellerTickets(manifestations[i]);
                        foreach(Tickets ticket in all_tickets)
                        {
                            tickets.Add(ticket);
                        }
                    }
                }
                else
                {
                    tickets = service.GetSellerTickets(user.MyManifestations);
                }
            }
            else
            {
                tickets = service.GetAllTickets();
            }

            if (name != null)
            {
                foreach (Tickets ticket in tickets)
                {
                    if (ticket.ReseverdManifestation.ToLower().Equals(name.ToLower()) || ticket.ReseverdManifestation.ToLower().Contains(name.ToLower()))
                    {
                        requested.Add(ticket);
                    }
                }
            }

            string[] dates = date.Split('-');
            DateTime startDate = DateTime.Parse(dates[0]);
            DateTime endDate = DateTime.Parse(dates[1]);
            foreach (Tickets ticket in tickets)
            {
                DateTime nowDate = DateTime.Parse(ticket.DateAndTimeManifestation);
                if (nowDate.Date >= startDate.Date && nowDate.Date <= endDate.Date)
                {
                   requested.Add(ticket);
                }

            }

            if (priceFrom == null)
            {
                priceFrom = "0";
            }

            if (priceTo != null)
            {
                double lower_price = Double.Parse(priceFrom);
                double higher_price = Double.Parse(priceTo);
                foreach (Tickets ticket in tickets)
                {
                    if (ticket.Price >= lower_price && ticket.Price < higher_price)
                    {
                        requested.Add(ticket);
                    }
                }
            }
            
            return requested;
        }
    }
}
