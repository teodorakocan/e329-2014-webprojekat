﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using MyFirstMVCWebApp.Models;

namespace MyFirstMVCWebApp.Service
{
    public class UserService : IUserService
    {
        private List<User> users = new List<User>();
        public UserService()
        {
            string path_C = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Customers.xml";
            XDocument doc_C = XDocument.Load(path_C);

            foreach (var node in doc_C.Descendants("Customer"))
            {
                User user = new User()
                {
                    Username = node.Element("Username").Value,
                    Password = node.Element("Password").Value,
                    Name = node.Element("Name").Value,
                    Surname = node.Element("Surname").Value,
                    Gender = node.Element("Gender").Value,
                    Type = node.Element("Type").Value,
                    DateOfBDay = node.Element("DateOfBirthday").Value,
                    Role = node.Element("Role").Value,
                    Points = double.Parse(node.Element("Points").Value),
                    Tickets = node.Element("Tickets").Value
                };
                users.Add(user);
            }

            string path_S = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Sellers.xml";
            XDocument doc_S = XDocument.Load(path_S);

            foreach (var node in doc_S.Descendants("Seller"))
            {
                User user = new User()
                {
                    Username = node.Element("Username").Value,
                    Password = node.Element("Password").Value,
                    Name = node.Element("Name").Value,
                    Surname = node.Element("Surname").Value,
                    Gender = node.Element("Gender").Value,
                    DateOfBDay = node.Element("DateOfBirthday").Value,
                    Role = node.Element("Role").Value,
                    MyManifestations = node.Element("MyManifestations").Value
                };
                users.Add(user);
            }

            string path_A = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Admins.xml";
            XDocument doc_A = XDocument.Load(path_A);

            foreach (var node in doc_A.Descendants("Admin"))
            {
                User user = new User()
                {
                    Username = node.Element("Username").Value,
                    Password = node.Element("Password").Value,
                    Name = node.Element("Name").Value,
                    Surname = node.Element("Surname").Value,
                    Gender = node.Element("Gender").Value,
                    DateOfBDay = node.Element("DateOfBirthday").Value,
                    Role = node.Element("Role").Value
                };
                users.Add(user);
            }
        }

        public List<User> GetUserList() => users;

        public User SearchByUsername(string username) {
            foreach(User user in users)
            {
                if(user.Username.Equals(username))
                {
                    return user;
                }
            }
            return null;
        }

        public User Validate(string username, string password)
        {
            List<User> allUsers = new List<User>();
            allUsers = GetUserList();
            foreach(User user in allUsers)
            {
                if(user.Username.Equals(username) && user.Password == password)
                {
                    return user;
                }
            }
            return null;
        }

        public List<User> GetSellersCustomers()
        {
            List<User> allUsers = new List<User>();
            foreach(User user in GetUserList())
            {
                if(user.Role.Equals("Seller") || user.Role.Equals("Customer"))
                {
                    allUsers.Add(user);
                }
            }
            return allUsers;
        }

        public List<Manifestation> GetManifestations()
        {
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Manifestations.xml";
            List<Manifestation> listOfEvents = new List<Manifestation>();
            XDocument doc = XDocument.Load(path);

            foreach (var node in doc.Descendants("Manifestation"))
            {
                Manifestation manifestation = new Manifestation()
                {
                    Name = node.Element("Name").Value,
                    Type = node.Element("Type").Value,
                    NumberOfPlaces = Int32.Parse(node.Element("NumberOfPlaces").Value),
                    NumberOfTickets = Int32.Parse(node.Element("NumberOfTickets").Value),
                    DateOfManifestation = node.Element("DateOfManifestation").Value,
                    PriceOfTicket = double.Parse(node.Element("PriceOfTicket").Value),
                    TimeOfManifestation = node.Element("TimeOfManifestation").Value,
                    Status = node.Element("Status").Value,
                    PlaceOfManifestation = new Place(),
                    Image = node.Element("Image").Value
                };

                string placeOfManifest = node.Element("PlaceOfManifestation").Value;
                string[] place = placeOfManifest.Split(',');
                manifestation.PlaceOfManifestation.Street_Num = place[1];
                manifestation.PlaceOfManifestation.City = place[0];
                manifestation.PlaceOfManifestation.ZipCode = Int32.Parse(place[2]);

                listOfEvents.Add(manifestation);
                listOfEvents.Sort((x, y) => DateTime.Compare(DateTime.Parse(x.DateOfManifestation), DateTime.Parse(y.DateOfManifestation)));
            }

            return listOfEvents;
        }
        public TypeOfUser SearchUserType(string type)
        {
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Types.xml";
            List<Manifestation> listOfEvents = new List<Manifestation>();
            XDocument doc = XDocument.Load(path);

            foreach (var node in doc.Descendants("Type"))
            {
                if(node.Element("Name").Value.Equals(type))
                {
                    TypeOfUser t = new TypeOfUser()
                    {
                        Name = node.Element("Name").Value,
                        Discount = Int32.Parse(node.Element("Discount").Value),
                        Points = Int32.Parse(node.Element("Points").Value)
                    };
                    return t;
                }
            }
            return null;
        }

        public Manifestation GetManifestation(string name)
        {
            List<Manifestation> listOfManifestation = new List<Manifestation>();
            listOfManifestation = GetManifestations();
            foreach(Manifestation man in listOfManifestation)
            {
                if(man.Name.Equals(name))
                {
                    return man;
                }
            }

            return null;
        }
        public double GetPrice(string user_type, double price)
        {
            TypeOfUser type = new TypeOfUser();
            type = SearchUserType(user_type);
            double discount = 0;
            double total_price = 0;
            discount = price / 100 * type.Discount;
            total_price += price - discount;
            return total_price;
        }

        public List<Tickets> GetAllTickets()
        {
            string path = @"C:\Users\Teodora\Desktop\Web\MyFirstMVCWebApp\MyFirstMVCWebApp\App_Data\Tickets.xml";
            List<Tickets> allTickets = new List<Tickets>();
            XDocument doc = XDocument.Load(path);

            foreach (var node in doc.Descendants("Ticket"))
            {
                Tickets ticket = new Tickets()
                {
                    ID = node.Element("ID").Value,
                    ReseverdManifestation = node.Element("ReseverdManifestation").Value,
                    DateAndTimeManifestation = node.Element("DateAndTimeManifestation").Value,
                    CustomerName = node.Element("CustomerName").Value,
                    Price = double.Parse(node.Element("Price").Value),
                    Status = node.Element("Status").Value,
                    Type = node.Element("Type").Value
                };
                allTickets.Add(ticket);
            }

            return allTickets;
        }

        public List<Tickets> GetCustomerTickets(string name)
        {
            List<Tickets> tickets = new List<Tickets>();
            List<Tickets> allTickets = new List<Tickets>();
            tickets = GetAllTickets();
            foreach(Tickets ticket in tickets)
            {
                if(ticket.CustomerName.Equals(name))
                {
                    if(ticket.Status.Equals("Rezervisano"))
                    {
                        allTickets.Add(ticket);
                    }
                }
            }

            return allTickets;
        }

        public List<Tickets> GetSellerTickets(string manifestaion)
        {
            List<Tickets> tickets = new List<Tickets>();
            List<Tickets> allTickets = new List<Tickets>();
            tickets = GetAllTickets();
            foreach (Tickets ticket in tickets)
            {
                if (ticket.ReseverdManifestation.Equals(manifestaion))
                {
                    allTickets.Add(ticket);
                }
            }

            return allTickets;
        }
    }
}