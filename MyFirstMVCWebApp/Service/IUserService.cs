﻿using MyFirstMVCWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstMVCWebApp.Service
{
    interface IUserService
    {
        User Validate(string username, string password);
        List<User> GetUserList();
        List<User> GetSellersCustomers();
        User SearchByUsername(string username);
        TypeOfUser SearchUserType(string type);
        List<Manifestation> GetManifestations();
        Manifestation GetManifestation(string name);
        double GetPrice(string user_type, double price);
        List<Tickets> GetAllTickets();
        List<Tickets> GetCustomerTickets(string name);
        List<Tickets> GetSellerTickets(string manifestaion);
    }
}
